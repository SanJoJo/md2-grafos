#include "Rii.h"

#define MAX(a,b) (((a)>(b))?(a):(b))

u32 Greedy(Grafo G){
  //Decoloreo el grafo.
  for(u32 i = 0;i < NumeroDeVertices(G); i++){
    G->nodos[i]->color=(u32) -1; //-1 en u32 ya que nunca llegamos a usar ese color.
  }
  u32 cant_C = 0;
  for (u32 i=0; i < NumeroDeVertices(G); i++){ //Con este for recorremos todos los vértices del grafo
    bool colores_usados [cant_C+1];
    for(u32 k = 0; k <cant_C+1; k++) colores_usados[k] = false;//Con este arreglo veo los colores que uso
    
    Vertice v = G->nodos[i]; // Me fijo en el vértice i
    
    for (u32 j = 0; j < v->grado; j++){ //Uso un for para ver que colores estan usados
      u32 c = v->vecinos[j]->color;
      if (c != (u32)-1){
        colores_usados[c] = true; //El valor true representa que esta usado
      }
    }
    //Busco el primer color en false (color no usado) dentro del arreglo de colores usados
    u32 color_no_usado = 0;
    
    while(colores_usados[color_no_usado] && color_no_usado < cant_C){ //Recorro el arreglo de colores usados
      color_no_usado++; //Aumento el una unidad la variable de color no usado
    }
    //Pinto el vértice con el color mas chico que puedo usar
    v->color = color_no_usado;
    if (color_no_usado == cant_C){
        cant_C++;
    }  
  }
  // La cantidad de colores luego de ejecutar el algoritmo es cant_C
  G->cant_C = cant_C;
  return cant_C;
}

int Bipartito(Grafo G){
	// En actual estara el vertice que estamos mirando y el vertice_queue el vertice ingresado en la cola.
  Vertice actual;
  Vertice vertice_queue;
	//Definimos la cola
  Cola queue; //creamos la cola con la cantidad de vértices el grafo
  
  //Decoloreo el grafo
  for(u32 i=0; i<NumeroDeVertices(G); i++){
    G->nodos[i]->color=(u32) -1;
  }
  
  u32 vertices_pintados = 0;
  
  while (vertices_pintados < NumeroDeVertices(G)){ // Si todavía no pintamos todos los vértices
      u32 num_vertice = 0;
      actual = G->nodos[num_vertice]; //Miro ese vértice.
      if (actual->color == (u32)-1){ // si el vértice actual no esta pintado
        queue = create_queue(NumeroDeVertices(G));
        actual->color = 0; // lo pinto con el color cero
        vertices_pintados++;
        enqueue(queue, actual); // agregamos el vértice que pintamos a la cola
        while(!is_empty(queue)) { //Corre el while mientras haya vértices en la cola (similar a un BFS)
          vertice_queue = dequeue(queue); //sacamos un elemento de la cola
          for(u32 i = 0; i < vertice_queue->grado; i++) { //Recorremos los vecinos del vértice asignado en el paso anterior
              if(vertice_queue->vecinos[i]->color == (u32)-1) { // Si el vecino no esta pintado
                  enqueue(queue, vertice_queue->vecinos[i]); // lo agregamos en la cola
                  vertice_queue->vecinos[i]->color = (1 - vertice_queue->color); // y lo pintamos con 0 ó 1
                  vertices_pintados++; //incremento los vértices que pinte
              }
          }
        }
        free_queue(queue); //Liberamos la memoria
      }
     
      num_vertice++;
  }

  
  /*Ahora hacemos un for para ver si efectivamente nuestro grafo tiene un coloreo propio,
	es decir, verificamos que un vértice tiene los colores distintos a sus vecinos
	*/
  
  Vertice v;
  
  for(u32 i = 0; i < NumeroDeVertices(G); i++) {
    v = G->nodos[i];
    for(u32 j = 0; j < v->grado; j++) {
      if(v->color == v->vecinos[j]->color){
        Greedy(G);
        return 0;
      }
    }
  }
  
  return 1;
}
