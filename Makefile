#---------------#
# INSTRUCCIONES #
#---------------#

# "make run" ejecuta el programa.
# "make valgrind" ejecuta los test con valgrind.
# "make debug" ejecuta el programa con gdb.
# "make clean" elimina los *.o y el ejecutable.

#-----------#
# VARIABLES #
#-----------#

# CC es el compilador que vamos a usar.
# Tambien se podria usar el compilador "cclang" o algun otro.
CC = gcc

# CCFLAGS son los flags que usaremos para compilar el programa.
# -IWahlaan ayuda a encontrar el HEADERS en el directorio Wahlaan.
# Para eliminar los mensajes de debug usar "-DNDEBUG".
CCFLAGS = -IWahlaan -Wall -Wextra -Werror -O3 -std=c99 -g

# Los archivos *.o necesarios para la cración del BINARY.
OBJECTS = cdc.o coloreo.o extractor.o nodos.o orden.o tadCola.o

# Estos HEADERS son los que se buscaran con la flag -IWahlaan.
HEADERS = Rii.h

# El archivo BINARY va a ser el ejecutable.
BINARY = main

#-----------#
# OBJETIVOS #
#-----------#

# Si uno solo escribe "make", se ejecuta all, ya que es el primer objetivo que aparece en el Makefile.
all: clean $(BINARY)

# Todos los archivos *.o se crearan en la carpeta principal.
# Los archivos *.c los buscara en el directorio Wahlaan.
# % es la forma que tiene el Makefile de hacer *, p.e. %.c es igual a *.c
# $? es una variable que en el momento que haga falta de reemplazara por Wahlaan/%.c
%.o: Wahlaan/%.c
	$(CC) $(CCFLAGS) -c $?

# $? funciona igual que antes, se reemplazara por main.c $(OBJECTS).
# $@ se reemplaza por el objetivo, en este caso "main".
main: main.c $(OBJECTS)
	$(CC) $(CCFLAGS) -o $@ $?

run: $(BINARY)
	./main

valgrind: $(BINARY)
	valgrind --show-reachable=yes --leak-check=full --tool=memcheck --track-origins=yes ./main

debug: $(BINARY)
	gdb ./main

clean:
	rm -f $(BINARY) $(OBJECTS) Wahlaan/*.gch *.gdb_history