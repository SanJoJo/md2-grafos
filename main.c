#include "Rii.h"

int main()
{
	// Variables para medir el tiempo.
    clock_t tiempoInicio;
    clock_t tiempoFinal;
  	double segundos;
  	
    tiempoInicio = clock(); // Inicia el tiempo.

  	/******************** Construccion del Grafo****************/
  	Grafo grafo = ConstruccionDelGrafo();
  	if(grafo == NULL){return 0;}

  	tiempoFinal = clock(); // Tomamos el tiempo transcurrido.
  	segundos = (double) (tiempoFinal-tiempoInicio)/CLOCKS_PER_SEC; // Calculamos los segundos.
  	printf("\nGrafo Creado en %f segundos.\n", segundos);
  	printf("El grafo tiene %u colores.\n", NumeroDeColores(grafo));
    if(Bipartito(grafo)) {
      printf("Es Bipartito.\n");
    }
    if (!Bipartito(grafo)){
      printf("No es Bipartito.\n");
      printf("Greedy pinto con %u colores.\n", NumeroDeColores(grafo));
    }
  	OrdenNatural(grafo);
  	printf("Greedy con Orden Natural == %u colores.\n", Greedy(grafo));
    

  	OrdenWelshPowell(grafo);
  	printf("Greedy con Orden WelshPowell == %u colores.\n", Greedy(grafo));
  	
  	u32 j = NumeroDeVertices(grafo);
    j = rand()%j;
  	for(u32 i=0; i<NumeroDeVertices(grafo); i++){
  		SwitchVertices(grafo,i,j);
  	}
  	printf("Greedy con SwitchVertices pinto con %u colores.\n", Greedy(grafo));
  	

  	RMBCnormal(grafo);
  	printf("Greedy con RMBCnormal pinto con %u colores.\n", Greedy(grafo));


  	RMBCrevierte(grafo);
  	printf("Greedy con RMBCrevierte pinto con %u colores.\n", Greedy(grafo));
    
    tiempoInicio = clock();
    Grafo ngrafo = CopiarGrafo(grafo);
    tiempoFinal = clock();
    segundos = (double) (tiempoFinal-tiempoInicio)/CLOCKS_PER_SEC;
    printf("\nSe copio el grafo en %f segundos.\n", segundos);
	DestruccionDelGrafo(grafo);
    DestruccionDelGrafo(ngrafo);
	
    return 0;
}
