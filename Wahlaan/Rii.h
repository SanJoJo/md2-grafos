/*https://www.geeksforgeeks.org/graph-coloring-set-2-greedy-algorithm/
Integrantes:

	Alumno: Balog Santiago Alberto
	Mail: santiagobalog1998@gmail.com

	Alumno: Farias Guillermo Joel
	Mail: joelfarias999@gmail.com

	Alumno: Molina Joel Edgardo
	Mail: jooelmolina21@gmail.com
*/

#ifndef _RII_H
#define _RII_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <assert.h>

typedef unsigned int u32;

//Estructura de vertices

typedef struct _vertice *Vertice;

struct _vertice{
    u32 nombre; // Nombre del Vertice.
    u32 hashDelV; // Etiqueta en la tabla de Hash
    u32 color; // Color que tiene el Vertice.
    u32 grado; // Cantidad de Vecinos.
    u32 numVecinos; // Numero de vecinos que tiene el vertice.
    Vertice *vecinos; // Puntero a los vecinos.
};

//Estructura del Grafo

typedef struct GrafoSt *Grafo;

struct GrafoSt{
    u32 cant_V;	// Cantidad de Vertices o Nodos
    u32 cant_A;	// Conjunto de Aristas o Lados.
    u32 cant_C;	// Cantidad de Colores.
    u32 verticesEnNodos;
    // El orden es el que se ingresa por stdin.
    Vertice *nodos; // Puntero a la estructura de vertices.
};


/*
Funciones De Construcción/Destrucción/Copia del grafo.
Archivo: cdc.c
*/
Grafo ConstruccionDelGrafo(); // Construye el grafo y lo aloca en la memoria corriendo Greedy y coloreando los vertices.

void DestruccionDelGrafo(Grafo G); // Destruye el grafo y libera la memoria.

Grafo CopiarGrafo(Grafo G); // Aloca memoria suficiente para un puntero, donde se copiara el grafo.

// Funciones auxiliares para cdc.c

bool EsNum(char supNum[]); // Verifica que todo un string sea un numero, en caso de serlo, lo castea a u32.

int agregarLado (Grafo G,u32 v1, u32 v2); // Agrega una arista nueva entre dos vertices o nodos al grafo.

Vertice ColocarVertice(Grafo G, u32 nombreV); // Coloca un nuevo vertice o nodo en el grafo.

u32 agregarVecinos(Vertice primero, Vertice segundo); // Agrega mutuamente a dos vertices o nodos como vecinos.


/*
Funciones de Coloreo.
Archivo: coloreo.c
*/
u32 Greedy(Grafo G); // Recorre el grafo utilizando el Algoritmo de Greedy.

int Bipartito(Grafo G); // Verifica que el grafo sea bipartito, si no lo es, lo colorea usando greedy.


/*
Especificación del TAD Cola para poder hacer la función Bipartito.
Archivo: tadCola.c
*/

typedef struct _cola_t *Cola;

Cola create_queue(u32 size); //Asignamos memoria

void empty_queue(Cola q); //Asigna los valores iniciales

bool is_empty(Cola q); //Nos dice si la cola esta vacia o no

void enqueue(Cola q, Vertice v); //Agrega un elemento a la cola

Vertice dequeue(Cola q); //Nos devuelve un elemento de la cola y lo desencola

void free_queue(Cola q); //Libera la memoria utilizada por la cola


/*
Funciones de extracción de datos del grafo.
Archivo: extractor.c

Las funciones de extract.c son todas de O(1).
*/
u32 NumeroDeVertices(Grafo G); // Cantidad de vertices o nodos de un grafo.

u32 NumeroDeLados(Grafo G);	// Cantidad de aristas o lados de un grafo.

u32 NumeroDeColores(Grafo G); // Cantidad de colores que usa el grafo.


/*
Funciones de los Vertices.
Archivo: nodos.c
*/
u32 NombreDelVertice(Grafo G, u32 i); // Nombre real del vertice o nodo i-esimo.

u32 ColorDelVertice(Grafo G, u32 i); // Color que tiene un vertice o nodo i-esimo.

u32 GradoDelVertice(Grafo G, u32 i); // Grado del vertice o nodo i-esimo.

u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j); // Color que tiene el vecino j-esimo del vertice o nodo j-esimo.

u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j); // Nombre del vecino j-esimo del vertice o nodo i-esimo.


/*
Funciones de Ordenación.
Archivo: orden.c
*/
char OrdenNatural(Grafo G);

int cmpOrdenNatural(const void *a, const void *b);

char OrdenWelshPowell(Grafo G);

int cmpWelshPowell(const void *a, const void *b);

char SwitchVertices(Grafo G,u32 i,u32 j);

char RMBCnormal(Grafo G);

int cmpRMBCnormal(const void *a, const void *b);

char RMBCrevierte(Grafo G);

int cmpRMBCrevierte(const void *a, const void *b);

char RMBCchicogrande(Grafo G);

int cmpRMBCchicogrande(const void *a, const void *b);

char SwitchColores(Grafo G,u32 i,u32 j);

#endif
