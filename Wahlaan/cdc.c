#include "Rii.h"

Grafo ConstruccionDelGrafo(){
    Grafo G = NULL; // Inicializamos el grafo.
    int tambuffer = 128; // Valor elegido por nosotros.
    char buffer[tambuffer]; // Creamos un buffer para la lectura del input.
    // Mientras empiece con 'c' el renglon (DIMACS), lo consideramos comentario y no hacemos nada.
    do{ // Leemos las lineas.
        if(fgets(buffer, tambuffer, stdin) == NULL){ // Si es NULL, no se leyo bien el input.
            printf("No se pudo procesar correctamente el input.\n");
            return NULL;
        }
    }while (buffer[0] == 'c'); // Sale del ciclo cuando ya no hayan mas comentarios.
    char p;
    char edge[4];
    char cant_VString[10]; // 10 es el largo del valor de (2^32)-1
    char cant_AString[10]; // 10 es el largo del valor de (2^32)-1
    // leemos del buffer la linea del input.
    sscanf(buffer,"%c %s %s %s", &p, edge, cant_VString, cant_AString);
    if(p != 'p'){ // Chequeamos que p sea el primer string del input.
        printf("El grafo ingresado no respeta DIMACS\n");
        return NULL;
    }
    if(strncmp(edge,"edge",4) != 0){ // Chequeamos que edge sea el segundo string del input.
        printf("%s %s %s\n", edge,cant_VString,cant_AString);
        printf("El grafo ingresado no respeta DIMACS\n");
        return NULL;
    }
    if(!EsNum(cant_AString) || !EsNum(cant_VString)){ // En caso de que alguna de las cantidades sea 0 devolvemos error.
        printf("Error en el argumento de Vertices\n");
        return NULL;
    }

    u32 cant_V = (u32)atoi(cant_VString); // Chequeamos que el tercer argumento sea un numero valido.
    u32 cant_A = (u32)atoi(cant_AString); // Chequeamos que el cuarto argumento sea un numero valido.

    if(cant_V < 2){
        printf("Error en primera linea sin comentarios.\n");
        return NULL;
    }
    if(cant_A == 0 || cant_A > (cant_V*(cant_V-1)/2)){
        printf("Error en primera linea sin comentarios.\n");
        return NULL;
    }
    G = malloc(sizeof(struct GrafoSt)); // Pedimos memoria para el grafo.
    if(G == NULL){
        printf("No se pudo pedir memoria para el grafo.\n");
        return NULL;
    }
    else{ // Inicializamos los valores de la estructura de grafo.
        G->cant_V = cant_V;
        G->cant_A = cant_A;
        G->cant_C = 0;
        G->verticesEnNodos = 0;
        G->nodos = calloc(cant_V,sizeof(Vertice)); // Pedimos memoria para los vertices.
        if(G->nodos == NULL){
            printf("No se pudo pedir memoria para los vertices del grafo.\n");
            free(G);
            return NULL;
        }
        for(u32 i=0; i<NumeroDeVertices(G); i++){
            G->nodos[i] = NULL;
        }
    }
    char vert1[10]; // 10 es el largo del valor de (2^32)-1
    char vert2[10]; // 10 es el largo del valor de (2^32)-1
    u32 ParseosCorrectos = 0;
    while(ParseosCorrectos < cant_A){ // En el ciclo se agregan cant_A de aristas.
        if(fgets(buffer, tambuffer, stdin) == NULL) { // Si es NULL, no se leyo bien el input.
            printf("Error en la lectura del input\n");
            // Como hubo un error liberamos la memoria.
            free(G->nodos);
            free(G);
            return NULL;
        }
        else {
            // leemos del buffer la linea del input.
            sscanf(buffer, "%c %s %s", &p, vert1, vert2);
            if(p != 'e'){ // Chequeamos que e sea el primer string del input.
                printf("Error agregando lados\n");
                // Como hubo un error liberamos la memoria.
                free(G->nodos);
                free(G);
                return NULL;
            }
            if(!EsNum(vert1) || !EsNum(vert2)){ // En caso de que alguno de los argumentos no sea un digito devolvemos error.
                printf("Error agregando lados\n");
                // Como hubo un error liberamos la memoria.
                free(G->nodos);
                free(G);
                return NULL;
            }

            u32 v1 = (u32)atoi(vert1);
            u32 v2 = (u32)atoi(vert2);
            
            int pudeAgregar = agregarLado(G,v1, v2); // Como no hubo error, se agraga la arista.
            if(pudeAgregar == -1){
                printf("No se pudo agregar un lado\n");
                DestruccionDelGrafo(G);
                return NULL;
            }
            ParseosCorrectos+=pudeAgregar;
        }
    }
    Greedy(G);
    return G; // Retornamos el grafo.
}

void DestruccionDelGrafo(Grafo G){
    assert(G != NULL);
    u32 cant_V = NumeroDeVertices(G); // Consultamos la cantidad de Vertices que tiene el grafo.
    for(u32 i=0; i<cant_V; i++){
        if(G->nodos[i] != NULL){
            if(G->nodos[i]->vecinos != NULL){
                free(G->nodos[i]->vecinos); // Liberamos la memoria donde guardamos a los vecinos de i.
            }
            free(G->nodos[i]); // Liberamos el vertice i.
        }
    }
    free(G->nodos); // Liberamos a los vertices del grafo.
    free(G); // Liberamos el grafo.
}

Grafo CopiarGrafo(Grafo G){
    Grafo NG = NULL; // Inicializo el nuevo grafo.
    if(G == NULL){
        printf("El grafo original es nulo.\n");
        return NULL;
    }
    NG = malloc(sizeof(struct GrafoSt)); // Pedimos memoria para el nuevo grafo.
    if(NG == NULL){
        printf("Hubo problema al pedir memoria para el nuevo grafo.\n");
        return NULL;
    }
    NG->cant_V = NumeroDeVertices(G); // Se copia el numero de vertices o nodos del grafo original.
    NG->cant_A = NumeroDeLados(G); // Se copia el numero de lados o aristas del grafo original.
    NG->cant_C = NumeroDeColores(G); // Se copia el numero de colores del grafo original.
    NG->verticesEnNodos = G->verticesEnNodos;
    NG->nodos = calloc(NG->cant_V,sizeof(Vertice)); // Pedimos memoria para los i nuevos vertices.
    if(NG->nodos == NULL){
        printf("No se pudo pedir memoria para los vertices del grafo.\n");
        free(NG);
        return NULL;
    }
    for(u32 i=0; i<NumeroDeVertices(NG); i++){ // Iteramos para construir los i vertices o nodos del nuevo grafo.
        NG->nodos[i] = malloc(sizeof(struct _vertice)); // Pedimos memoria para la estructura del vertice o nodo nuevo.
        if(NG->nodos[i] == NULL){
            printf("No se pudo pedir memoria para el vertice.\n");
            DestruccionDelGrafo(NG);
            return NULL;
        }
        NG->nodos[i]->nombre = NombreDelVertice(G,i); // Copiamos el nombre del i-esimo vertice o nodo.
        NG->nodos[i]->hashDelV = G->nodos[i]->hashDelV;
        NG->nodos[i]->color = ColorDelVertice(G,i); // Copiamos el color del i-esimo vertice o nodo.
        NG->nodos[i]->grado = GradoDelVertice(G,i); // Copiamos el grado del i-esimo vertice o nodo.
        NG->nodos[i]->vecinos = calloc(NG->nodos[i]->numVecinos, sizeof(Vertice));// Pedimos memoria para el arreglo de vecinos del i-esimo vertice o nodo.
        if(NG->nodos[i]->vecinos == NULL){
            printf("No se pudo pedir memoria para los vecinos.\n");
            DestruccionDelGrafo(NG);
            return NULL;
        }
        for(u32 j=0; j<NG->nodos[i]->numVecinos; j++){
            NG->nodos[i]->vecinos[j]->nombre = NombreJotaesimoVecino(G,i,j); // Copiamos los vecinos del vertice i.
            NG->nodos[i]->vecinos[j]->color = ColorJotaesimoVecino(G,i,j);
            NG->nodos[i]->vecinos[j]->hashDelV = G->nodos[i]->vecinos[j]->hashDelV;
            NG->nodos[i]->vecinos[j]->grado = G->nodos[i]->vecinos[j]->grado;
            NG->nodos[i]->vecinos[j]->numVecinos = G->nodos[i]->vecinos[j]->numVecinos;
            NG->nodos[i]->vecinos[j]->vecinos = G->nodos[i]->vecinos[j]->vecinos;
        }
    }
	return NG; // Retornamos el nuevo grafo.
};

/* Funciones auxiliares para la construccion del grafo. */

bool EsNum(char supNum[]){
    u32 tam = strlen(supNum); // Largo que tiene el numero.
    for(u32 i=0; i<tam; i++){
        char supDigit = supNum[i]; // Copia el digito en la posicion i.
        int check = atoi(&supDigit); // Lo transforma en int, y lo copia.
        if(check<0 || check>9){ // Chequeamos que el entero sea un valor entre 0 y 9.
            return false; // Retornamos -1 en caso de error.
        }
    }
    return true; // Retornamos el valor casteado en u32.
}

int agregarLado(Grafo G, u32 v1, u32 v2){
    Vertice primero = ColocarVertice(G,v1); // Coloca el nuevo vertice en el Grafo, salvo que ya exista.
    Vertice segundo = ColocarVertice(G,v2); // Coloca el nuevo vertice en el Grafo, salvo que ya exista.
    if(primero == NULL || segundo == NULL){ // Chequeamos que los dos vertices esten en el grafo.
        return (-1);
    }
    
    u32 status1 = agregarVecinos(primero,segundo); // Se agregan ambos vertices como vecinos.
    u32 status2 = agregarVecinos(segundo,primero);
    if(status1 == 0 && status2 == 0){ // Chequeamos que todo haya ido bien.
        return 0;
    }

    return 1; // Retornamos 1 si todo fue bien.
}

Vertice ColocarVertice(Grafo G, u32 nombreV){
    u32 cantVertices = G->cant_V; // Guardamos en una variable temporal la cantidad de vertices del grafo.
    u32 index = (nombreV % cantVertices); // Utilizamos la función HASH para saber donde guardar el nuevo vertice.
    if(G->nodos[index] == 0){ // Si es igual a-1, significa que el lugar esta disponible.
        Vertice nuevo = malloc(sizeof(struct _vertice));
        if(nuevo == NULL){
            printf("No se pudo pedir memoria para los vecinos del vertice %u\n", nombreV);
            return NULL;
        }
        nuevo->nombre = nombreV; // Guardamos el nombre del vertice.
        nuevo->hashDelV = index;
        nuevo->grado = 0; // Como recien se crea el vertice tiene grado 0.
        nuevo->color = 0;
        nuevo->numVecinos = 1;
        nuevo->vecinos = (Vertice*) malloc(sizeof(Vertice)); // Se pide memoria para los vecinos.
        if(nuevo->vecinos == NULL){
            printf("No se pudo pedir memoria para los vecinos del vertice %u\n", nombreV);
            free(nuevo);
            return NULL;
        }
        G->nodos[index] = nuevo;
        G->verticesEnNodos = G->verticesEnNodos+1;
        return nuevo; // Se retorna la posición en que guarde el vértice
    }

    if((G->nodos[index]->nombre) == nombreV){ // En caso de que ya exista.
        return G->nodos[index]; // Retornamos el mismo vertice.
    }

    u32 newIndex = 0;
    for (u32 search=1; search<cantVertices; search++){ // Iteración para hayar el proximo lugar libre.
        newIndex = (index + search) % cantVertices;
        if(G->nodos[newIndex] == 0){ // Chequeamos que el espacio este disponible.
            Vertice nuevo = malloc(sizeof(struct _vertice)); // Pedimos memoria para la estructura del nuevo vertice.
            if(nuevo == NULL){
                printf("No se pudo pedir memoria para los vecinos del vertice %u\n", nombreV);
                return NULL;
            }
            nuevo->nombre = nombreV; // Guardamos el nombre del vertice.
            nuevo->hashDelV = newIndex;
            nuevo->grado = 0; // Como recien se crea el vertice tiene grado 0.
            nuevo->color = 0; // El coloreo va a ser el lugar asignado al terminar de crear el grafo.
            nuevo->numVecinos = 1;
            nuevo->vecinos = (Vertice*)calloc(nuevo->numVecinos,sizeof(Vertice)); // Se pide memoria para los vecinos.
            if(nuevo->vecinos == NULL){
                printf("No se pudo pedir memoria para los vecinos del vertice %u\n", nombreV);
                free(nuevo);
                return NULL;
            }
            G->nodos[newIndex] = nuevo;
            G->verticesEnNodos = G->verticesEnNodos+1;
            return nuevo; // Se retorna el vertice ya agregado al grafo.
        }
        if(G->nodos[newIndex]->nombre == nombreV){ // En caso de que ya exista.
            return G->nodos[newIndex]; // Retornamos el mismo vertice.
        }
    }
    printf("No se pudo guardar el Vértice %u en el grafo\n",nombreV);
    // Por alguna razón no pudo guardar el vertice.
    return NULL;
}

/* Funciones que ayuda a crear el graf

o */

u32 agregarVecinos(Vertice primero, Vertice segundo){
    // Agregamos a segundo como el vecino de primero
    if (primero->grado >= primero->numVecinos){
	   primero->numVecinos *= 2;
	   primero->vecinos = realloc(primero->vecinos, primero->numVecinos * sizeof(Vertice));
	   if(primero->vecinos == NULL){
        	printf("Fallo la reasignación de memoria para el vertice %u\n", primero->nombre);
       		return 0;
        }
    }  
    
    primero->vecinos[primero->grado] = segundo; // Guardamos al segundo vertice como vecino del primero.
	primero->grado++; // Aumentamos el grado del primer vertice.
    return 1; // Retornamos 1 si todo fue bien.
}
