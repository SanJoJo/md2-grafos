#include "Rii.h"

u32 NombreDelVertice(Grafo G, u32 i){
	assert(G != NULL);
	assert(G->nodos[i] != NULL);
    return ((G->nodos[i])->nombre);
}

u32 ColorDelVertice(Grafo G, u32 i){
	assert(G != NULL);
	assert(G->nodos[i] != NULL);
    return ((G->nodos[i])->color);
}

u32 GradoDelVertice(Grafo G, u32 i){
	assert(G != NULL);
	assert(G->nodos[i] != NULL);
    return ((G->nodos[i])->grado);
}

u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j){
	assert(G != NULL);
	assert(G->nodos[i] != NULL);
	assert(G->nodos[i]->vecinos[j] != NULL);
	return (((G->nodos[i])->vecinos[j])->color);
}

u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j){
	assert(G != NULL);
	assert(G->nodos[i] != NULL);
	assert(G->nodos[i]->vecinos[j] != NULL);
	return (((G->nodos[i])->vecinos[j])->nombre);
}
