#include "Rii.h"

u32 NumeroDeVertices(Grafo G){
	assert(G != NULL);
	return (G->cant_V);
}

u32 NumeroDeLados(Grafo G){
	assert(G != NULL);
	return (G->cant_A);
}

u32 NumeroDeColores(Grafo G){
	assert(G != NULL);
	return (G->cant_C);
}
