#include "Rii.h"

/*Ordena los vértices en orden creciente de sus “nombres” reales.
Retorna 0 si todo anduvo bien, 1 si hubo algún problema.
*/
char OrdenNatural(Grafo G){
  if (G==NULL){
    return 1;
  }
  qsort(G->nodos, NumeroDeVertices(G), sizeof(Vertice), cmpOrdenNatural);
  return 0;
}

/*Función de comparación. Recibe dos punteros genericos nulos.*/
int cmpOrdenNatural(const void *a, const void *b){
  Vertice v1 = *(Vertice*)a;
  Vertice v2 = *(Vertice*)b;
  if (v1->nombre > v2->nombre){ //Retorna 1 si queres orden ascendente, -1 si queres orden descendente
    return 1;
  }
  
  if (v1->nombre == v2->nombre) {  //Retorna -1 si queres orden ascendente, 1 si queres orden descendente
    return 0;
  }
  
  return -1;
}

/*Ordena los vértices según los grados en orden no creciente.
Retorna 0 si todo anduvo bien, 1 si hubo algún problema.
*/
char OrdenWelshPowell(Grafo G){
  if (G==NULL){
    return 1;
  }
  qsort(G->nodos, NumeroDeVertices(G), sizeof(Vertice), cmpWelshPowell);
  return 0;
}

/*Función de comparación. Recibe dos punteros genericos nulos.*/
int cmpWelshPowell(const void *a, const void *b){
  Vertice v1 = *(Vertice*)a;
  Vertice v2 = *(Vertice*)b;
  if (v1->grado < v2->grado){  //Retorna -1 si queres orden ascendente, 1 si queres orden descendente
    return 1;
  }
  if (v1->grado == v2->grado){ //Retorna 1 si queres orden ascendente, -1 si queres orden descendente
    return 0;
  }
  return -1;
}

/*Si ambos estan en el intervalo permitido, entonces
intercambia las posiciones de los vértices en los lugares
i y j del orden interno de G.
Verifica que i, j <número de vértices. Si no es cierto, retorna 1.
*/
char SwitchVertices(Grafo G,u32 i,u32 j){
  if (G==NULL){
    return '1';
  }
  if (i < NumeroDeVertices(G) && j < NumeroDeVertices(G)){
    Vertice tmp = G->nodos[i];
    G->nodos[i] = G->nodos[j];
    G->nodos[j]=tmp;
    return '0';
  }
  else {
    return '1';
  }
}

/*Si G esta coloreado con r colores
para todo i = 0, .., r−2 los vértices de VCi estan todos antes de los vértices de VCi+1 .
*/
char RMBCnormal(Grafo G){
  if (G==NULL){
    return '1';
  }
  qsort(G->nodos, NumeroDeVertices(G), sizeof(Vertice), cmpRMBCnormal);
  return '0';
}

/*Función de comparación. Recibe dos punteros genericos nulos.*/
int cmpRMBCnormal(const void *a, const void *b){
  Vertice v1 = *(Vertice*)a;
  Vertice v2 = *(Vertice*)b;
  if (v1->color > v2->color){ //Retorna 1 si queres orden ascendente, -1 si queres orden descendente
    return 1;
  }
  else if (v1->color < v2->color) { //Retorna -1 si queres orden ascendente, 1 si queres orden descendente
    return -1;
  }
  else {
    return 0;
  }
}

/*Si G esta coloreado con r colores
para todo i = 0, .., r−2 los vertices de VCi estan todos luego de los vertices de VCi+1 .
*/
char RMBCrevierte(Grafo G){
  if (G==NULL){
    return '1';
  }
  qsort(G->nodos, NumeroDeVertices(G), sizeof(Vertice), cmpRMBCrevierte);
  return '0';
}

/*Función de comparación. Recibe dos punteros genericos nulos.*/
int cmpRMBCrevierte(const void *a, const void *b){
  Vertice v1 = *(Vertice*)a;
  Vertice v2 = *(Vertice*)b;
  if (v1->color > v2->color){ //Retorna 1 si queres orden ascendente, -1 si queres orden descendente
    return -1;
  }
  else if (v1->color < v2->color) { //Retorna -1 si queres orden ascendente, 1 si queres orden descendente
    return 1;
  }
  else {
    return 0;
  }
}

/*Si G esta coloreado con r colores y VC1 son los vértices coloreados con 1, VC2 los coloreados con 2, etc, entonces esta
función ordena los vértices de chico a grande teniendo en cuenta la cantidad de vértices pintados con un determinado color
*/
//Declaro el arreglo fuera de la función para que pueda ser utilizado en la funcion de comparación
u32 *colores;
char RMBCchicogrande(Grafo G){
  if (G==NULL){
    return '1';
  }
  colores = calloc (NumeroDeColores(G),sizeof(u32)); //Asignamos memoria
  if (colores == NULL){ //Comprueba que la memoria se asigno correctamente. Si hubo error retornamos 1
    return '1';
  }
  for(u32 i=0;i<NumeroDeVertices(G);i++){
    colores[ColorDelVertice(G, i)]++; //Aumentamos en 1 cuando haya un vértice de ese color
  }
  qsort(G->nodos, NumeroDeVertices(G), sizeof(Vertice), cmpRMBCchicogrande);
  free(colores); // Liberamos la memoria que habiamos asignado
  return '0';
}


/*Función de comparación. Recibe dos punteros genericos nulos.*/
int cmpRMBCchicogrande(const void *a, const void *b){
  Vertice v1 = *(Vertice*)a;
  Vertice v2 = *(Vertice*)b;
  if (colores[v1->color] > colores[v2->color]){ //Retorna 1 si queres orden ascendente, -1 si queres orden descendente
    return 1;
  }
  else if (colores[v1->color] < colores[v2->color]){ //Retorna -1 si queres orden ascendente, 1 si queres orden descendente
    return -1;
  }
  else {
    if (v1->color > v2->color){ //Retorna 1 si queres orden ascendente, -1 si queres orden descendente
      return 1;
    }
    else if (v1->color < v2->color){ //Retorna -1 si queres orden ascendente, 1 si queres orden descendente
      return -1;
    }
    else {
      return 0;
    }
  }
}

/*Intercambia los colores i, j: todos los vértices que esten coloreados en el coloreo actual con i pasan a tener
el color j en el nuevo coloreo y los que están coloreados con j en el coloreo actual pasan a tener el color i en el nuevo coloreo.
Verifica que i, j <número de colores que tiene G en ese momento. Si no es cierto, retorna 1.
*/
char SwitchColores(Grafo G,u32 i,u32 j){
  if (G==NULL){
    return '1';
  }
  if (i < NumeroDeColores(G) && j < NumeroDeColores(G)){
    //Tenemos que ver los colores de cada vértice. k indica el vértice que estamos viendo
    for(u32 k=0; k< NumeroDeVertices(G);k++){
      if (ColorDelVertice(G,k) == i){ //Si el color del vértice k es igual a i
        G->nodos[k]->color = j; //Cambio el color a j
      }
      else if (ColorDelVertice(G,k) == j){ //Si el color del vértice es igual a j
        G->nodos[k]->color = i; //Cambio el color a i
      }
    }
    return '0';
  }
  else {
    return '1';
  }
}
