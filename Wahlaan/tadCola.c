#include "Rii.h"

struct _cola_t {
	Vertice *vertexs;
	int first;
	int last;
};

Cola create_queue(u32 size) {
	Cola q = malloc(1 * sizeof(struct _cola_t)); //Asignamos memoria
	q->vertexs = malloc(size * sizeof(Vertice));
	empty_queue(q);
	return q;
}

void empty_queue(Cola q){
	q->first = -1;
	q->last = -1;
}

bool is_empty(Cola q) {
	return(q->first == q->last); //Si el primer elemento es igual al ultimo esta vacia
}

void enqueue(Cola q, Vertice v) {
	++q->last; //Movemos la posición del ultimo elemento
	q->vertexs[q->last] = v;
}

Vertice dequeue(Cola q) {
	if(is_empty(q))
		return NULL; //Devolvemos NULL si la cola esta vacia
	++q->first; //Establecemos el primer elemento de la cola
	Vertice v = q->vertexs[q->first];
	return v; // Retonarmos el vértice
}

void free_queue(Cola q) {
	free(q->vertexs);
	free(q);
}